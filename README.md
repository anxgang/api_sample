## Gem

1. [figaro](https://github.com/laserlemon/figaro)
2. [rest-client](https://github.com/rest-client/rest-client)

## 安裝 figaro

```
gem "figaro"
```

```
$ bundle
```

```
$ figaro install
```


## 安裝 RestClient

```
gem 'rest-client'
```

```
$ bundle
```

## 說明

application.yml 設定好裏面的 url

```yml
# 預設值為：
linebot_api_url: https://whatever.com

# 表示如果是 production 的話 linebot_api_url 改用這個 url
production:
  linebot_api_url: http://production.com

```

使用方式 只要再任意地方取得 `ENV["linebot_api_url"]`
這個值會隨著 development 、 test 或 production 改變

只要在 application.yml 指定好即可

```rb
def index
  # development: https://whatever.com
  # test:        https://whatever.com
  # production:  http://production.com
  url = ENV["linebot_api_url"]
  res = RestClient.get(url)

  render json: res
end
```