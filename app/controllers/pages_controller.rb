class PagesController < ApplicationController
  def index
    url = ENV["linebot_api_url"]
    res = RestClient.get(url)
    
    render json: res
  end
end
